//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser= require('body-parser')
app.use(bodyparser.json())
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimieNtosJSON = require('./movimientosv2.json');
app.get('/', function (req,res) {
  //res.send('Hola Mundo nodejs');
  res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/', function (req,res) {
  res.send('Hemos recibido su peticion POST');
})


app.get('/clientes', function (req,res) {
  //res.send('Hola Mundo nodejs');
  res.send('Aqui estan los clientes');
});

app.get('/clientes/:idcliente', function (req,res) {
  //res.send('Hola Mundo nodejs');
  res.send('Aqui tiene al clientes numero:'+ req.params.idcliente);
});

app.get('/Movimientos/v1', function (req,res) {
  //res.send('Hola Mundo nodejs');
  res.sendfile('movimientosv1.json');
});

app.get('/Movimientos/v2/:index', function (req,res) {
  //res.send('Hola Mundo nodejs');
  console.log(req.params.index);
  res.send(movimieNtosJSON[req.params.index-1]);
  //res.sendfile('movimientosv2.json');
});

app.get('/Movimientosq/v2', function (req,res) {
  //res.send('Hola Mundo nodejs');
  console.log(req.query);
  res.send('recibido');
  //res.sendfile('movimientosv2.json');
});

app.post('/Movimientos/v2', function (req,res) {
  //res.send('Hola Mundo nodejs');
  var  nuevo = req.body
  nuevo.id = movimieNtosJSON.length + 1;
  movimieNtosJSON.push(nuevo)
    res.send('Movimiento dado de alta');
  //res.sendfile('movimientosv2.json');
});

app.get('/Movimientos/v2', function (req,res) {
  //res.send('Hola Mundo nodejs');
  res.send(movimieNtosJSON);
});

//Hola
